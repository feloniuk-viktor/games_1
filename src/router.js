import {createRouter, createWebHashHistory} from "vue-router";

// import Home from "@/pages/Home";
import Game from "@/pages/Game";
import User from "@/pages/User";

export default createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            component: () => import('@/pages/Home'),
            name: 'home',
        },
        {
            path: '/game',
            component: Game,
            name: 'game-page'
        },
        {
            path: '/user/:id',
            component: User,
            name: 'user-page'
        }
    ]
})
/*vue 2
import Vue from 'vue'
import VueRouter from "vue-router";

Vue.use(VueRouter)

export default new VueRouter({})*/
