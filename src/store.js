// import Vuex from 'vuex'
import {createStore} from 'vuex'
const store = createStore({
    state:()=>({
        player_1_name:'',
        player_2_name:'',
    }),
    mutations:{
        setPlayer1Name(state,name){
            state.player_1_name=name

        },
        setPlayer2Name(state,name){
            state.player_2_name=name
        }
    },
    getters:{
        players(state){
            return [state.player_1_name, state.player_2_name]
        }
    },
    actions:{},
})
export default store
